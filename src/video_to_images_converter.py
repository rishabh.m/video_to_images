#!/usr/bin/env python


import cv2

import rospy
from cv_bridge import CvBridge, CvBridgeError

from sensor_msgs.msg import Image


class VideoToImagesConverter:
    bridge = CvBridge()

    frame_number = 0
    save_image_number = 1

    def __init__(self, images_save_folder_, frames_skip_):
        self.frames_skip = frames_skip_
        self.images_save_folder = images_save_folder_[-1] == "/" and images_save_folder_[:-1] or images_save_folder_

        self.sub = rospy.Subscriber(
            rospy.get_param(rospy.resolve_name("~video_stream_topic")), 
            Image, 
            self.callback)

    def convert_image(self, data):
        try:
            return self.bridge.imgmsg_to_cv2(data)
        except CvBridgeError as e:
            print('video_to_images: ' + e)

    def callback(self, image_data):
        self.frame_number += 1

        if self.frame_number % self.frames_skip == 0:
            cv_image = self.convert_image(image_data)

            if cv_image is not None:
                cv2.imwrite(self.images_save_folder + "/" + str(self.save_image_number) + ".jpg", cv_image)
                self.save_image_number += 1

            rospy.loginfo(rospy.get_caller_id()
                          + ': Saved image number ' + str(self.save_image_number)
                          + ' from frame ' + str(self.frame_number))


if __name__ == '__main__':
    try:
        rospy.init_node('video_to_images_converter')
        video_to_image_converter = VideoToImagesConverter(
            rospy.get_param(rospy.resolve_name("~images_save_folder")),
            rospy.get_param(rospy.resolve_name("~frames_skip"))
        )
        rospy.spin()
    except rospy.ROSInterruptException as e:
        print(e)
