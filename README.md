# Installing this package

The code in this repository is essentially a catkin package. So, navigate to your catkin workspace and clone this repo there:

    cd /path/to/catkin_ws/src
    git clone <this_repo>


# Installing the Dependencies

## ROS Dependencies

This package requires the [ros-driver/video_stream_opencv](https://github.com/ros-drivers/video_stream_opencv) to be cloned and built in your catkin workspace:

    cd /path/to/catkin_ws/src
    git clone https://github.com/ros-drivers/video_stream_opencv.git
    cd ../
    catkin_make

Refer to the [wiki for this ROS package](http://wiki.ros.org/video_stream_opencv) for any further clarifications.

## External C++ Dependencies

This package also requires OpenCV to be installed on your system and that it be CMake discoverable. The CMakeLists.txt file in this package depends on it through this line:
    
    find_package(OpenCV REQUIRED)

If you run into no problems when you `catkin_make` the workspace containing this package, then your OpenCV installation is fine, and you don't have to worry.


# Using this package

The conversion can be done from the `video_stream_to_images.launch` file:

    roslaunch video_to_images video_stream_to_images.launch \
    video_file:=/full/path/to/input/video \
    images_save_folder:=/path/to/folder/where/you/want/to/save/the/images \
    frames_skip:=5

Explanation of the arguments:
1. `video_file` - You need to provide the absolute path to the video file you want to convert.
2. `images_save_folder` - The folder name where the images will be saved. *This is a sub-path* that will start at
`video_to_images/data/`, and you will have to create the path beforehand.
For example, if you give the value as `images_save_folder:=IKEA_images`, you will have to create the folder
`video_to_images/data/IKEA_images` beforehand.
3.  `frames_skip` - This is the number of frames that are skipped between saves. Default value is `5`, meaning every fifth frame is saved as an image. 
